import 'package:flutter/material.dart';

class TeamMember extends StatelessWidget {
  final String image;
  final String firstName;
  final String lastName;
  final String collegeName;
  TeamMember({this.image, this.firstName, this.lastName, this.collegeName});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 30.0),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
            child: Image.asset(
              image,
              fit: BoxFit.cover,
              height: 170.66666,
              width: 128.0,
            ),
          ),
          SizedBox(
            width: 25.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                firstName,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                  color: Color(0xFF656176),
                ),
              ),
              Text(
                lastName,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                  color: Color(0xFF656176),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                collegeName,
                style: TextStyle(
                  color: Color(0xFF807B96),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
