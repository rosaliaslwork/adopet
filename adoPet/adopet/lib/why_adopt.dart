import 'package:adopet/menu_page.dart';
import 'package:flutter/cupertino.dart';
import 'constants.dart';
import 'package:flutter/material.dart';

class WhyAdoptPage extends StatefulWidget {
  @override
  _WhyAdoptPageState createState() => _WhyAdoptPageState();
}

class _WhyAdoptPageState extends State<WhyAdoptPage> {
  List<String> whyAdoptText = [kWhyAdoptText1, kWhyAdoptText2, kWhyAdoptText3];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  color: Color(0xFFe6e4ef),
                  padding: EdgeInsets.only(left: 15.0, top: 50.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, MenuPage.id);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Color(0xFF656176),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset(
                      'images/why-adopt-image.png',
                      height: 350.0,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0),
                      ),
                      color: Color(0xFFe6e4ef),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topCenter,
                      child: Text(
                        'Why Adopt ?',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 36.0,
                          fontFamily: 'Pacifico',
                          letterSpacing: 2.0,
                          color: Color(0xFF656176),
                        ),
                      ),
                    ),
                    Divider(
                      indent: 60.0,
                      endIndent: 60.0,
                      color: Color(0xFF656176),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: ListView(
                      controller: ScrollController(
                        initialScrollOffset: 40.0,
                      ),
                      children: <Widget>[
                        ListTile(
                          title: Text(
                            kWhyAdoptText1,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: Color(0xFF807B96),
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            kWhyAdoptText2,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Color(0xFF807B96),
                            ),
                          ),
                        ),
                        ListTile(
                          contentPadding: EdgeInsets.only(
                              bottom: 15.0, right: 10.0, left: 16.0),
                          title: Text(
                            kWhyAdoptText3,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Color(0xFF807B96),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
//               Container(
//                  child: Text(kWhyAdoptText1),
//                ),
//                Container(
//                  child: Text(kWhyAdoptText2),
//                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
