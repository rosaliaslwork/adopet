import 'package:adopet/menu_page.dart';
import 'package:flutter/material.dart';
import 'package:adopet/team_member.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AboutUs extends StatelessWidget {
  static String id = 'about-us';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: 50.0,
              left: 20.0,
              bottom: 30.0,
            ),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, MenuPage.id);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Color(0xFF656176),
                    size: 30.0,
                  ),
                ),
                SizedBox(
                  width: 15.0,
                ),
                Text(
                  'Our Team',
                  style: TextStyle(
                    fontSize: 35.0,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF656176),
                  ),
                ),
              ],
            ),
          ),
          TeamMember(
            collegeName: 'Indira Gandhi Delhi\nTechnical University\nfor Women',
            firstName: 'SRISHTI',
            lastName: 'VASHISTHA',
            image: 'images/srishti-aboutus.jpg',
          ),
          SizedBox(
            height: 25.0,
          ),
          TeamMember(
            collegeName: 'Sri Ramaswamy Memorial\nUniversity, Amaravati',
            firstName: 'LAASYA',
            lastName: 'CHOUDARY',
            image: 'images/laasya-aboutus.jpeg',
          ),
          SizedBox(
            height: 25.0,
          ),
          TeamMember(
            collegeName: 'Indira Gandhi Delhi\nTechnical University\nfor Women',
            firstName: 'ROSALIA',
            lastName: 'LONGJAM',
            image: 'images/rosalia-aboutus.jpeg',
          ),
          Padding(
            padding: EdgeInsets.only(left: 30.0, top: 30.0),
            child: Text(
              'Built with',
              style: TextStyle(
                fontSize: 17.0,
                color: Colors.grey[700],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  left: 30.0,
                  top: 10.0,
                ),
                child: Image.asset(
                  'images/Google-flutter-logo.png',
                  height: 35.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0, right: 20.0),
                child: Image.asset(
                  'images/transparent-adopet-logo.png',
                  height: 50.0,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
