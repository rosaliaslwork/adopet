import 'package:adopet/pet_profile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'favourites_pet_card.dart';

class PetCard extends StatelessWidget {
  PetCard({this.screenHeight, this.deviceWidth, this.pets});
  final double screenHeight;
  final double deviceWidth;
  final List pets;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          itemCount: pets.length,
          itemBuilder: (context, index) {
            final animal = pets[index];
            return GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return PetProfile(animal: animal);
                }));
              },
              child: Padding(
                padding: EdgeInsets.only(
                  bottom: screenHeight * 0.01231,
                  right: deviceWidth * 0.05333,
                  left: deviceWidth * 0.05333,
                ),
                child: Stack(
                  alignment: Alignment.centerLeft,
                  children: <Widget>[
                    Material(
                      color: Colors.grey[100],
                      borderRadius:
                          BorderRadius.circular(screenHeight * 0.02463),
                      elevation: screenHeight * 0.00492,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: screenHeight * 0.02463,
                          horizontal: deviceWidth * 0.05333,
                        ),
                        child: Row(
                          children: <Widget>[
                            SizedBox(
                              width: deviceWidth * 0.37333,
                            ),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Text(
                                        animal.name,
                                        style: TextStyle(
                                          fontSize: screenHeight * 0.02586,
                                          fontFamily: 'Pacifico',
                                          letterSpacing: screenHeight * 0.00246,
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFF656176),
                                        ),
                                      ),
                                      Icon(
                                        (animal.gender == 'Female')
                                            ? FontAwesomeIcons.venus
                                            : FontAwesomeIcons.mars,
                                        color: Color(0xFF656176),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    animal.breed,
                                    style: TextStyle(
                                      color: Color(0xFF807B96),
                                    ),
                                  ),
                                  Text(
                                    '${animal.ageNumber} ${animal.ageUnit} old',
                                    style: TextStyle(
                                      color: Color(0xFF807B96),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          height: screenHeight * 0.23399,
                          width: deviceWidth * 0.4,
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.circular(screenHeight * 0.02463),
                          ),
                        ),
                        ClipRRect(
                          borderRadius:
                              BorderRadius.circular(screenHeight * 0.02463),
                          child: Image(
                            image: AssetImage(animal.image),
                            height: screenHeight * 0.20935,
                            fit: BoxFit.cover,
                            width: deviceWidth * 0.4,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
