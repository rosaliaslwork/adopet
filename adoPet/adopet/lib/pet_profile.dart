import 'constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'selected_photo.dart';
import 'pet_profile_data.dart';
import 'animal.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'mail.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PetProfile extends StatefulWidget {
  PetProfile({this.animal});
  final Animal animal;
  @override
  _PetProfileState createState() => _PetProfileState();
}

class _PetProfileState extends State<PetProfile> {
  final _firestore = Firestore.instance;
  final _auth = FirebaseAuth.instance;
  FirebaseUser loggedInUser;
  String alertMessage;
  String alertTitle;
  bool isFavourite;

  int photoIndex = 0;
  @override
  void initState() {
    super.initState();
    getCurrentUser();
    getEmail();

//    isFavourited('${loggedInUser.uid}${widget.animal.id}');
  }

  void getCurrentUser() async {
    try {
      final user = await _auth.currentUser();
      if (user != null) {
        loggedInUser = user;

        isFavourited('${loggedInUser.uid}${widget.animal.id}');
      }
    } catch (e) {
      print(e);
    }
  }

  String email = '';

  Future getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      email = preferences.getString('email');
    });
    print(email);
  }

  void isFavourited(String docId) async {
    await for (var snapshot
        in _firestore.collection('favourites').snapshots()) {
      for (var favourite in snapshot.documents) {
        if (favourite.documentID == docId) {
          isFavourite = favourite.exists;
          break;
        }
      }
    }
    isFavourite = false;
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  setState(() {
                    photoIndex = (photoIndex + 1) % widget.animal.photos.length;
                  });
                },
                child: Container(
                  width: double.infinity,
                  height: screenHeight * 0.5,
                  child: Image.asset(
                    widget.animal.photos[photoIndex],
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.only(
                    left: screenHeight * 0.0197, top: screenHeight * 0.04926),
                height: screenHeight * 0.5,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                  ),
                ),
              ),
              PetName(
                screenHeight: screenHeight,
                petName: widget.animal.name,
              ),
              Positioned(
                bottom: screenHeight * 0.0862,
                child: SelectedPhoto(
                  numberOfDots: widget.animal.photos.length,
                  photoIndex: photoIndex,
                ),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight * 0.00985,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(
                left: screenHeight * 0.00985,
                right: screenHeight * 0.00985,
              ),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  PetLocation(
                    screenHeight: screenHeight,
                    location: widget.animal.location,
                  ),
                  SizedBox(
                    height: screenHeight * 0.01231,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      GenderCard(
                        screenHeight: screenHeight,
                        genderIcon: (widget.animal.gender == 'Female')
                            ? FontAwesomeIcons.venus
                            : FontAwesomeIcons.mars,
                        gender: widget.animal.gender,
                      ),
                      AgeWeightCard(
                        screenHeight: screenHeight,
                        numberText: widget.animal.ageNumber,
                        unit: widget.animal.ageUnit,
                        description: 'Age',
                      ),
                      BreedCard(
                        screenHeight: screenHeight,
                        breedLine1: widget.animal.breedline1,
                        breedLine2: widget.animal.breedline2,
                      ),
                      AgeWeightCard(
                        screenHeight: screenHeight,
                        numberText: widget.animal.weightNumber,
                        unit: widget.animal.weightUnit,
                        description: 'Weight',
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight * 0.01231,
                  ),
                  ShelterOwnerInfo(
                    screenHeight: screenHeight,
                    imagePath: widget.animal.ownerImagePath,
                    ownerName: widget.animal.ownerName,
                    category: widget.animal.ownerCategory,
                  ),
                  SizedBox(
                    height: screenHeight * 0.01231,
                  ),
                  OwnerShelterDescription(
                    screenHeight: screenHeight,
                    description: widget.animal.ownerStatement,
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: screenHeight * 0.136,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xFFe6e4ef),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(screenHeight * 0.03078),
                  topRight: Radius.circular(screenHeight * 0.03078),
                )),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    setState(() {
                      widget.animal.toggleFavourite();
                      isFavourited('${loggedInUser.uid}${widget.animal.id}');
                      print(isFavourite);
                    });

                    if (widget.animal.isFavourite) {
                      _firestore
                          .collection('favourites')
                          .document('${loggedInUser.uid}${widget.animal.id}')
                          .setData({
                        'name': widget.animal.name,
                        'breed': widget.animal.breed,
                        'ageUnit': widget.animal.ageUnit,
                        'ageNumber': widget.animal.ageNumber,
                        'gender': widget.animal.gender,
                        'image': widget.animal.image,
                        'user-id':
                            loggedInUser.uid != null ? loggedInUser.uid : email,
                      });
                    } else {
                      _firestore
                          .collection('favourites')
                          .document('${loggedInUser.uid}${widget.animal.id}')
                          .delete();
                    }
                  },
                  child: Container(
                      height: screenHeight * 0.07389,
                      width: screenHeight * 0.07389,
                      decoration: BoxDecoration(
                        color: Color(0xFF656176),
                        borderRadius:
                            BorderRadius.circular(screenHeight * 0.01847),
                      ),
                      child: widget.animal.isFavourite
                          ? kActiveHeartIcon
                          : kInactiveHeartIcon),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      widget.animal.toggleAdopted();
                    });
                    if (widget.animal.isAdopted) {
                      _firestore
                          .collection('adoption_requests')
                          .document('${loggedInUser.uid}${widget.animal.id}')
                          .setData({
                        'name': widget.animal.name,
                        'breed': widget.animal.breed,
                        'ageUnit': widget.animal.ageUnit,
                        'ageNumber': widget.animal.ageNumber,
                        'gender': widget.animal.gender,
                        'image': widget.animal.image,
                        'user-id':
                            loggedInUser.uid != null ? loggedInUser.uid : email,
                      });
                      sendMail(loggedInUser.email, widget.animal.name, false);
                      alertTitle = 'Thank You!';
                      alertMessage =
                          'We\'ll notify ${widget.animal.ownerName} that you are interested in adopting ${widget.animal.name}';
                    } else {
                      sendMail(loggedInUser.email, widget.animal.name, true);
                      alertTitle = 'Warning';
                      alertMessage = 'We are withdrawing your adoption request';
                      _firestore
                          .collection('adoption_requests')
                          .document('${loggedInUser.uid}${widget.animal.id}')
                          .delete();
                    }
                    Alert(
                      context: context,
                      title: alertTitle,
                      desc: alertMessage,
                      buttons: [
                        DialogButton(
                          child: Text(
                            'Okay',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          color: Color(0xFF656176),
                        ),
                      ],
                    ).show();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: screenHeight * 0.07389,
                    width: screenHeight * 0.29556,
                    decoration: BoxDecoration(
                      color: widget.animal.isAdopted
                          ? kActiveAdoptionColor
                          : kInactiveAdoptionColor,
                      borderRadius:
                          BorderRadius.circular(screenHeight * 0.01847),
                    ),
                    child: Text(
                      'Adoption',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: screenHeight * 0.02463,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
