import 'package:adopet/menu_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'constants.dart';
import 'button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  static String id = 'login_screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _auth = FirebaseAuth.instance;
  bool showSpinner = false;
  String email;
  String password;
  bool _isLoggedIn = false;
  String error;

  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  _login() async {
    try {
      setState(() {
        showSpinner = true;
      });
      final googleUser = await _googleSignIn.signIn();
      if (googleUser != null) {
        final GoogleSignInAuthentication googleSignInAuthentication =
            await googleUser.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken,
        );

        final AuthResult authResult =
            await _auth.signInWithCredential(credential);
        final FirebaseUser user = authResult.user;
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setString('email', googleUser.displayName);
        Navigator.pushNamed(context, MenuPage.id);
        setState(() {
          _isLoggedIn = true;
          showSpinner = false;
        });
      } else {
        setState(() {
          showSpinner = false;
        });
        Alert(
          context: context,
          title: 'Wrong Email/Password!',
          desc:
              'Your email or password is incorrect. Please recheck them and try again.',
          buttons: [
            DialogButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              color: Color(0xFF656176),
            ),
          ],
        ).show();
      }
    } catch (err) {
      setState(() {
        showSpinner = false;
      });

      print(err);
    }
  }

  _logout() {
    _googleSignIn.signOut();
    setState(() {
      _isLoggedIn = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    height: screenHeight * 0.12315,
                  ),
                  Positioned(
                    top: screenHeight * 0.06157,
                    left: screenWidth * 0.06666,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Color(0xFF656176),
                        size: screenHeight * 0.03325,
                      ),
                    ),
                  ),
                ],
              ),
              Center(
                child: Container(
                  height: screenHeight * 0.27709,
                  child: Image.asset('images/login-screen.png'),
                ),
              ),
              SizedBox(
                height: screenHeight * 0.04926,
              ),
              Padding(
                padding: EdgeInsets.only(left: screenWidth * 0.05333),
                child: Text(
                  'Welcome,',
                  style: TextStyle(
                      color: Color(0xFF656176),
                      fontWeight: FontWeight.bold,
                      fontSize: screenHeight * 0.0431),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: screenWidth * 0.05333),
                child: Text(
                  'Sign in to continue!',
                  style: TextStyle(
                    color: Color(0xFF807B96),
                    fontSize: screenHeight * 0.02832,
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight * 0.04926,
              ),
              Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: screenWidth * 0.06666),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  textAlign: TextAlign.left,
                  decoration: kTextFieldDecoration.copyWith(
                    hintText: 'Enter your email',
                  ),
                  onChanged: (value) {
                    email = value;
                  },
                ),
              ),
              SizedBox(
                height: screenHeight * 0.01847,
              ),
              Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: screenWidth * 0.06666),
                child: TextField(
                  obscureText: true,
                  textAlign: TextAlign.left,
                  decoration: kTextFieldDecoration.copyWith(
                    hintText: 'Enter your password',
                  ),
                  onChanged: (value) {
                    password = value;
                  },
                ),
              ),
              SizedBox(
                height: screenHeight * 0.02463,
              ),
              Center(
                child: Button(
                  buttonTitle: 'Login',
                  color: Color(0xFF807B96),
                  textColor: Colors.white,
                  onPressed: () async {
                    setState(() {
                      showSpinner = true;
                    });
                    try {
                      final user = await _auth.signInWithEmailAndPassword(
                          email: email, password: password);
                      if (user != null) {
                        SharedPreferences preferences =
                            await SharedPreferences.getInstance();
                        preferences.setString('email', email);
                        Navigator.pushNamed(context, MenuPage.id);
                        setState(() {
                          showSpinner = false;
                        });
                      }
                    } catch (e) {
                      setState(() {
                        showSpinner = false;
                      });
                      if ('$e' ==
                          'PlatformException(ERROR_WRONG_PASSWORD, The password is invalid or the user does not have a password., null)') {
                        error =
                            'The password you entered is incorrect. Please try again.';
                      } else if ('$e' ==
                          'PlatformException(ERROR_USER_NOT_FOUND, There is no user record corresponding to this identifier. The user may have been deleted., null)') {
                        error =
                            'There is no user record corresponding to this email. Please try again.';
                      } else if ('$e' ==
                          'PlatformException(ERROR_INVALID_EMAIL, The email address is badly formatted., null)') {
                        error =
                            'The email address that you entered is invalid. Please try again.';
                      } else {
                        error =
                            'Your details are incorrect. Please recheck them and try again.';
                      }
                      Alert(
                        context: context,
                        title: 'Something went wrong.',
                        desc: error,
                        buttons: [
                          DialogButton(
                            child: Text(
                              'Cancel',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: Color(0xFF656176),
                          ),
                        ],
                      ).show();

                      print(e);
                    }
                  },
                ),
              ),
              SizedBox(
                height: screenHeight * 0.01847,
              ),
              Center(
                child: Text(
                  'or sign in with',
                  style: TextStyle(color: Color(0xFF807B96)),
                ),
              ),
              SizedBox(
                height: screenHeight * 0.01231,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    _login();
                  },
                  child: Image.asset(
                    'images/google.png',
                    height: screenHeight * 0.03694,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
