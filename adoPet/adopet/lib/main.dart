import 'package:adopet/about_us.dart';
import 'package:adopet/adoption_requests_page.dart';
import 'package:adopet/favourites.dart';
import 'package:adopet/login_screen.dart';
import 'package:adopet/menu_page.dart';
import 'package:adopet/registration-screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'why_adopt.dart';
import 'package:flutter/services.dart';
import 'pet_profile.dart';
import 'side_bar.dart';
import 'animal.dart';
import 'constants.dart';
import 'user_profile.dart';
import 'welcome_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var email = preferences.getString('email');
  runApp(AdoPet(
    email: email,
  ));
}

class AdoPet extends StatelessWidget {
  var email;
  AdoPet({this.email});
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
        theme: ThemeData.light().copyWith(
          scaffoldBackgroundColor: Colors.white,
        ),
        initialRoute: email == null ? WelcomeScreen.id : MenuPage.id,
        routes: {
          WelcomeScreen.id: (context) => WelcomeScreen(),
          LoginScreen.id: (context) => LoginScreen(),
          RegistrationScreen.id: (context) => RegistrationScreen(),
          MenuPage.id: (context) => MenuPage(),
          FavouritesPage.id: (context) => FavouritesPage(),
          AdoptionRequestsPage.id: (context) => AdoptionRequestsPage(),
          AboutUs.id: (context) => AboutUs(),
        });
  }
}
