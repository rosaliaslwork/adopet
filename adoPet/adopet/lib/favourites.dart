import 'package:adopet/menu_page.dart';
import 'package:adopet/pet_profile.dart';
import 'package:flutter/material.dart';
import 'user_profile.dart';
import 'favourites_pet_card.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'animal.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:empty_widget/empty_widget.dart';

class FavouritesPage extends StatefulWidget {
  static String id = 'favourites-page';
  @override
  _FavouritesPageState createState() => _FavouritesPageState();
}

class _FavouritesPageState extends State<FavouritesPage> {
  final _firestore = Firestore.instance;
  final _auth = FirebaseAuth.instance;
  FirebaseUser loggedInUser;

  void FavouritesStream() async {
    await for (var snapshot
        in _firestore.collection('favourites').snapshots()) {
      for (var favourite in snapshot.documents) {
        print(favourite.data['isFavourite']);
      }
    }
  }

  void getCurrentUser() async {
    final user = await _auth.currentUser();
    try {
      if (user != null) {
        loggedInUser = user;
//        print(loggedInUser.email);
      }
    } catch (e) {
      print(e);
    }
  }

  String email = '';

  Future getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      email = preferences.getString('email');
    });
    print(email);
  }

  @override
  void initState() {
    super.initState();
    FavouritesStream();
    getCurrentUser();
    getEmail();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color(0xFFe6e4ef),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.05541,
                left: screenWidth * 0.02666,
                right: screenWidth * 0.02666,
                bottom: screenHeight * 0.01231),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, MenuPage.id);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Color(0xFF656176),
                  ),
                ),
                Text(
                  'Favourites',
                  style: TextStyle(
                    fontSize: screenHeight * 0.03448,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF656176),
//                    fontFamily: 'Roboto',
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return UserProfile();
                    }));
                  },
                  child: Image.asset(
                    'images/adopet-logo.png',
                    height: 40.0,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(screenHeight * 0.03078),
                    topLeft: Radius.circular(screenHeight * 0.03078)),
              ),
              child: StreamBuilder<QuerySnapshot>(
                stream: _firestore.collection('favourites').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.blueAccent,
                      ),
                    );
                  }
                  final favourites = snapshot.data.documents;
                  List<GestureDetector> favouriteWidgets = [];
                  for (var favourite in favourites) {
                    var userID =
                        loggedInUser != null ? loggedInUser.uid : email;
                    if (favourite.data['user-id'] == userID) {
                      Animal animal1;
                      final name = favourite.data['name'];
                      final breed = favourite.data['breed'];
                      final ageNumber = favourite.data['ageNumber'];
                      final ageUnit = favourite.data['ageUnit'];
                      final image = favourite.data['image'];
                      final gender = favourite.data['gender'];
                      for (var animal in allAnimals) {
                        if (animal.name == name) {
                          animal1 = animal;
                        }
                      }

                      final favouriteWidget = GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return PetProfile(
                              animal: animal1,
                            );
                          }));
                        },
                        child: FavouritesPetCard(
                          screenHeight: screenHeight,
                          screenWidth: screenWidth,
                          name: name,
                          breed: breed,
                          ageNumber: ageNumber,
                          ageUnit: ageUnit,
                          image: image,
                          gender: gender,
                        ),
                      );
                      favouriteWidgets.add(favouriteWidget);
                    }
                  }
                  return favouriteWidgets.length != 0
                      ? ListView(
                          children: favouriteWidgets,
                        )
                      : EmptyListWidget(
                          title: 'No favourites',
                          subTitle: 'You haven\'t liked anything yet',
                          image: 'images/loading-ghost.png',
                          titleTextStyle: Theme.of(context)
                              .typography
                              .dense
                              .headline4
                              .copyWith(color: Color(0xFF807B96)),
                          subtitleTextStyle: Theme.of(context)
                              .typography
                              .dense
                              .bodyText1
                              .copyWith(color: Color(0xFF807B96)));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
