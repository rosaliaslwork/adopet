import 'package:adopet/constants.dart';
import 'package:adopet/pet_profile.dart';
import 'package:adopet/side_bar.dart';
import 'package:adopet/about_us.dart';
import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'pet_category_card.dart';
import 'package:fluttericon/rpg_awesome_icons.dart';
import 'animal.dart';
import 'pet_card.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'favourites_pet_card.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum WidgetMaker {
  dog,
  cat,
  bird,
  rabbit,
}

class MenuPage extends StatefulWidget {
  static String id = 'menu-page';

  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  final _auth = FirebaseAuth.instance;
  FirebaseUser loggedInUser;
  var scaffoldKey = GlobalKey<ScaffoldState>();
  Future<List<Animal>> search(String search) async {
    await Future.delayed(Duration(seconds: 1));
    List<Animal> matches = [];
    for (var animal in allAnimals) {
      if (animal.breed.toLowerCase().contains(search.toLowerCase())) {
        matches.add(animal);
      }
    }
    return matches;
  }

  @override
  void initState() {
    super.initState();
    getEmail();
    getCurrentUser();
  }

  void getCurrentUser() async {
    final user = await _auth.currentUser();
    try {
      if (user != null) {
        loggedInUser = user;
//        print(loggedInUser.email);
      }
    } catch (e) {
      print(e);
    }
  }

  String email = '';

  Future getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      email = preferences.getString('email');
    });
    print(email);
  }

  WidgetMaker selectedWidgetMarker = WidgetMaker.dog;
  Color selectedCardColor = kInactiveCategoryCardColor;
  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMaker.dog:
        return getDogContainer();
      case WidgetMaker.cat:
        return getCatContainer();
      case WidgetMaker.rabbit:
        return getRabbitContainer();
      case WidgetMaker.bird:
        return getBirdContainer();
    }
    return getDogContainer();
  }

  Widget getDogContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: dogs,
    );
  }

  Widget getCatContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: cats,
    );
  }

  Widget getRabbitContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: bunnies,
    );
  }

  Widget getBirdContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: birds,
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      key: scaffoldKey,
      drawer: Drawer(
        child: SideBar(),
      ),
      backgroundColor: Color(0xFFe6e4ef),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.05541,
                left: screenHeight * 0.01231,
                right: screenHeight * 0.01231,
                bottom: screenHeight * 0.01231),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    scaffoldKey.currentState.openDrawer();
                  },
                  child: Icon(
                    FontAwesomeIcons.bars,
                    color: Color(0xFF656176),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, AboutUs.id);
                  },
                  child: Image.asset(
                    'images/adopet-logo.png',
                    height: 40.0,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(screenHeight * 0.03078),
                    topLeft: Radius.circular(screenHeight * 0.03078)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: screenHeight * 0.01231,
                        vertical: screenHeight * 0.01231,
                      ),
                      child: SearchBar<Animal>(
                        onSearch: search,
                        cancellationWidget: Icon(Icons.close),
                        onError: (error) {
                          return Center(
                            child: Text("Error occurred : $error"),
                          );
                        },
                        emptyWidget: Center(
                          child: Text("No search results"),
                        ),
                        onItemFound: (Animal animal, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return PetProfile(
                                  animal: animal,
                                );
                              }));
                            },
                            child: FavouritesPetCard(
                              screenHeight: screenHeight,
                              screenWidth: screenWidth,
                              name: animal.name,
                              ageUnit: animal.ageUnit,
                              ageNumber: animal.ageNumber,
                              image: animal.image,
                              gender: animal.gender,
                              breed: animal.breed,
                            ),
                          );
                        },
                        hintText: 'Search breed',
                        searchBarStyle: SearchBarStyle(
                          borderRadius: BorderRadius.all(
                            Radius.circular(screenHeight * 0.01847),
                          ),
                        ),
                        placeHolder: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker = WidgetMaker.dog;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: FontAwesomeIcons.dog,
                                    petLabel: 'Dog',
                                    color: (selectedWidgetMarker ==
                                            WidgetMaker.dog)
                                        ? kActiveCategoryCardColor
                                        : kInactiveCategoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMaker.dog)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMaker.dog)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker = WidgetMaker.cat;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: FontAwesomeIcons.cat,
                                    petLabel: 'Cat',
                                    color: (selectedWidgetMarker ==
                                            WidgetMaker.cat)
                                        ? kActiveCategoryCardColor
                                        : kInactiveCategoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMaker.cat)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMaker.cat)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker = WidgetMaker.rabbit;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: RpgAwesome.rabbit,
                                    petLabel: 'Rabbit',
                                    color: (selectedWidgetMarker ==
                                            WidgetMaker.rabbit)
                                        ? kActiveCategoryCardColor
                                        : kInactiveCategoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMaker.rabbit)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMaker.rabbit)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker = WidgetMaker.bird;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: FontAwesomeIcons.crow,
                                    petLabel: 'Bird',
                                    color: (selectedWidgetMarker ==
                                            WidgetMaker.bird)
                                        ? kActiveCategoryCardColor
                                        : kInactiveCategoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMaker.bird)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMaker.bird)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              child: getCustomContainer(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
