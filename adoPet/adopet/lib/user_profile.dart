import 'package:adopet/menu_page.dart';
import 'package:flutter/material.dart';

class UserProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xFF656176),
        body: SafeArea(
            child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 15.0, top: 5.0),
              alignment: Alignment.topLeft,
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, MenuPage.id);
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              height: 100.0,
            ),
            CircleAvatar(
              radius: 100.0,
              backgroundImage: AssetImage('images/rosalia.png'),
            ),
            Text(
              'Rosalia',
              style: TextStyle(
                fontFamily: 'Pacifico',
                fontSize: 40.0,
                color: Color(0xFFe6e4ef),
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'FUTURE PET OWNER',
              style: TextStyle(
                fontFamily: 'Source Sans Pro',
                color: Color(0xFFe6e4ef),
                fontSize: 20.0,
                letterSpacing: 2.5,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 20.0,
              width: 150.0,
              child: Divider(
                color: Color(0xFFe6e4ef),
              ),
            ),
            Card(
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                child: ListTile(
                  leading: Icon(
                    Icons.email,
                    color: Color(0xFF807B96),
                  ),
                  title: Text(
                    'rosalia@email.com',
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Color(0xFF807B96),
                        fontFamily: 'Source Sans Pro'),
                  ),
                )),
            Card(
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                child: ListTile(
                  leading: Icon(
                    Icons.phone,
                    color: Color(0xFF807B96),
                  ),
                  title: Text(
                    '+91 99999 99999',
                    style: TextStyle(
                      color: Color(0xFF807B96),
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                    ),
                  ),
                )),
          ],
        )),
      ),
    );
  }
}
