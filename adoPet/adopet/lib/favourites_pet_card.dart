import 'package:adopet/pet_profile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'animal.dart';

class FavouritesPetCard extends StatelessWidget {
  FavouritesPetCard({
    this.screenHeight,
    this.screenWidth,
    this.name,
    this.gender,
    this.breed,
    this.image,
    this.ageNumber,
    this.ageUnit,
  });

  final double screenHeight;
  final double screenWidth;
  final String name;
  final String gender;
  final String breed;
  final String ageNumber;
  final String ageUnit;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: screenHeight * 0.01231,
        right: screenWidth * 0.05333,
        left: screenWidth * 0.05333,
      ),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: <Widget>[
          Material(
            color: Colors.grey[100],
            borderRadius: BorderRadius.circular(screenHeight * 0.02463),
            elevation: screenHeight * 0.00492,
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: screenHeight * 0.02463,
                horizontal: screenWidth * 0.05333,
              ),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: screenWidth * 0.37333,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Flexible(
                              child: Text(
                                name,
                                style: TextStyle(
                                  fontSize: screenHeight * 0.02586,
                                  fontFamily: 'Pacifico',
                                  letterSpacing: screenHeight * 0.00246,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF656176),
                                ),
                              ),
                            ),
                            Icon(
                              (gender == 'Female')
                                  ? FontAwesomeIcons.venus
                                  : FontAwesomeIcons.mars,
                              color: Color(0xFF656176),
                            ),
                          ],
                        ),
                        Text(
                          breed,
                          style: TextStyle(
                            color: Color(0xFF807B96),
                          ),
                        ),
                        Text(
                          '$ageNumber $ageUnit old',
                          style: TextStyle(
                            color: Color(0xFF807B96),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Stack(
            children: <Widget>[
              Container(
                height: screenHeight * 0.23399,
                width: screenWidth * 0.4,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(screenHeight * 0.02463),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(screenHeight * 0.02463),
                child: Image(
                  image: AssetImage(image),
                  height: screenHeight * 0.20935,
                  fit: BoxFit.cover,
                  width: screenWidth * 0.4,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
