# adoPet

An app made using Flutter for pet adoption.

## Description:
The app contains list of various pets, their breeds, etc. It is a way of connecting shelters to users. 
A lot of people are interested in adopting pets. However, to find the perfect pet, they have to visit several shelters.
Our app provides a solution to this problem. Anyone can find their companion from the comfort of their home using our app. 

### This is the logo of our app
<img src="Images/logo.png" width="200" height="200" />

## App Screens:

### Welcome Page

<img src="Images/WelcomeScreen.png" width="200" height="420" />

### Registration Page

<img src="Images/RegistrationScreen.png" width="200" height="420" />            

### Registration Page Alerts

<img src="Images/InvalidEmail.png" width="200" height="420" /> <img src="Images/WeakPassword.png" width="200" height="420" /> <img src="Images/AlreadyRegisteredUser.png" width="200" height="420" /> 

### Login Page

<img src="Images/LoginScreen.png" width="200" height="420" />            

### Login Page Alerts

<img src="Images/InvalidEmail.png" width="200" height="420" /> <img src="Images/WrongPassword.png" width="200" height="420" /> <img src="Images/UnregisteredUser.png" width="200" height="420" /> 

### Menu Page

<img src="Images/MenuPage.png" width="200" height="420" />    

### Search Bar

<img src="Images/SearchBar.png" width="200" height="420" />  

### No Search Results

<img src="Images/NoSearchResults.png" width="200" height="420" />  

### Pet Profile 

<img src="Images/PetProfile.png" width="200" height="420" /> 

### Adoption Alerts

<img src="Images/AdoptionAlert.png" width="200" height="420" />  <img src="Images/AdoptionWithdrawal.png" width="200" height="420" />  


### Side Bar

<img src="Images/SideBar.png" width="200" height="420" />  

### No Adoption Requests

<img src="Images/NoAdoptionRequests.png" width="200" height="420" /> 

### Adoption Requests

<img src="Images/AdoptionRequests.png" width="200" height="420" /> 

### No Favourites

<img src="Images/NoFavourites.png" width="200" height="420" /> 

### Favourites

<img src="Images/Favourites.png" width="200" height="420" /> 

### Why Adopt Page

<img src="Images/WhyAdopt.png" width="200" height="420" /> 

### About Us Page

<img src="Images/AboutUs.png" width="200" height="420" /> 










 
