import 'package:adopetapp/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'selected_photo.dart';
import 'pet_profile_data.dart';
import 'constants.dart';
import 'animal_class.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'email.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PetProfile extends StatefulWidget {
  PetProfile({this.animal});
  final Animal animal;
  @override
  _PetProfileState createState() => _PetProfileState();
}

class _PetProfileState extends State<PetProfile> {
  final _firestore = Firestore.instance;
  FirebaseUser loggedInUser;
  final _auth = FirebaseAuth.instance;
//  Icon selectedIcon = kInactiveHeartIcon;
//  Color selectedColor = kInactiveAdoptionColor;
  String alertTitle;
  String alertMessage;
  bool isFavourited = false;

  var userId;

  void getFirebaseUserId() async {
    await for (var snapshot
        in _firestore.collection('favourites').snapshots()) {
      for (var favourite in snapshot.documents) {
        userId = favourite.data['user-id'];
      }
    }
  }

//  bool isFavourite;
//  bool isAdopted;

  int photoIndex = 0;

  void getCurrentUser() async {
    final user = await _auth.currentUser();
    try {
      if (user != null) {
        loggedInUser = user;
        print(loggedInUser.email);
      }
    } catch (e) {
      print(e);
    }
  }

  String email = '';

  Future getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      email = preferences.getString('email');
    });
    print(email);
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
    getEmail();
    getFirebaseUserId();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  setState(() {
                    photoIndex = (photoIndex + 1) % widget.animal.photos.length;
                  });
                },
                child: Container(
                  height: screenHeight * 0.5,
                  width: double.infinity,
                  child: Image.asset(
                    widget.animal.photos[photoIndex],
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: screenHeight * 0.04926, left: screenHeight * 0.0197),
                alignment: Alignment.topLeft,
                height: screenHeight * 0.5,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
//                    size: screenHeight * 0.0394,
                  ),
                ),
              ),
              PetName(
                screenHeight: screenHeight,
                petName: widget.animal.name,
              ),
              Positioned(
                bottom: screenHeight * 0.0862,
                child: SelectedPhoto(
                  numberOfDots: widget.animal.photos.length,
                  photoIndex: photoIndex,
                ),
              )
            ],
          ),
          SizedBox(
            height: screenHeight * 0.00985,
          ),
          Expanded(
            child: Container(
                padding: EdgeInsets.only(
                    left: screenHeight * 0.00985,
                    right: screenHeight * 0.00985),
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    PetLocation(
                      screenHeight: screenHeight,
                      location: widget.animal.location,
                    ),
                    SizedBox(
                      height: screenHeight * 0.01231,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        GenderCard(
                          screenHeight: screenHeight,
                          genderIcon: (widget.animal.gender == 'Female')
                              ? FontAwesomeIcons.venus
                              : FontAwesomeIcons.mars,
                          gender: widget.animal.gender,
                        ),
                        AgeWeightCard(
                          screenHeight: screenHeight,
                          numberText: widget.animal.ageNumber,
                          unit: widget.animal.ageUnit,
                          description: 'Age',
                        ),
                        BreedCard(
                          screenHeight: screenHeight,
                          breedLine1: widget.animal.breedline1,
                          breedLine2: widget.animal.breedline2,
                        ),
                        AgeWeightCard(
                          screenHeight: screenHeight,
                          numberText: widget.animal.weightNumber,
                          unit: widget.animal.weightUnit,
                          description: 'Weight',
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight * 0.01231,
                    ),
                    ShelterOwnerInfo(
                      screenHeight: screenHeight,
                      imagePath: widget.animal.ownerImagePath,
                      ownerName: widget.animal.ownerName,
                      category: widget.animal.ownerCategory,
                    ),
                    SizedBox(
                      height: screenHeight * 0.01231,
                    ),
                    OwnerShelterDescription(
                      screenHeight: screenHeight,
                      description: widget.animal.ownerStatement,
                    ),
                  ],
                )),
          ),
          Container(
            height: screenHeight * 0.136,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xFFe6e4ef),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(screenHeight * 0.03078),
                    topRight: Radius.circular(screenHeight * 0.03078))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    setState(() {
                      widget.animal.toggleFavourite();
                    });
                    if (widget.animal.isFavourite) {
                      _firestore
                          .collection('favourites')
                          .document('${loggedInUser.uid}${widget.animal.id}')
                          .setData({
                        'name': widget.animal.name,
                        'breed': widget.animal.breed,
                        'ageUnit': widget.animal.ageUnit,
                        'ageNumber': widget.animal.ageNumber,
                        'gender': widget.animal.gender,
                        'image': widget.animal.image,
                        'user-id':
                            loggedInUser.uid != null ? loggedInUser.uid : email,
                      });
                    } else {
                      if (loggedInUser.uid == userId) {
                        _firestore
                            .collection('favourites')
                            .document('${loggedInUser.uid}${widget.animal.id}')
                            .delete();
                      }
                    }
                  },
                  child: Container(
//                    margin: EdgeInsets.only(left: 20),
                    decoration: BoxDecoration(
                        color: Color(0xFF656176),
                        borderRadius:
                            BorderRadius.circular(screenHeight * 0.01847)),
                    height: screenHeight * 0.07389,
                    width: screenHeight * 0.07389,
                    child: widget.animal.isFavourite
                        ? kActiveHeartIcon
                        : kInactiveHeartIcon,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      widget.animal.toggleAdopted();
                    });
                    if (widget.animal.isAdopted) {
                      _firestore
                          .collection('adoption_requests')
                          .document('${loggedInUser.uid}${widget.animal.id}')
                          .setData({
                        'name': widget.animal.name,
                        'breed': widget.animal.breed,
                        'ageUnit': widget.animal.ageUnit,
                        'ageNumber': widget.animal.ageNumber,
                        'gender': widget.animal.gender,
                        'image': widget.animal.image,
                        'user-id':
                            loggedInUser.uid != null ? loggedInUser.uid : email,
                      });
                      sendMail(loggedInUser.email, widget.animal.name, false);
                      alertTitle = 'Thank you!';
                      alertMessage =
                          'We will notify ${widget.animal.ownerName} that you are interested in adopting ${widget.animal.name}';
                    } else {
                      sendMail(loggedInUser.email, widget.animal.name, true);
                      alertTitle = 'Warning';
                      alertMessage = 'We are withdrawing your adoption request';
                      if (loggedInUser.uid == userId) {
                        _firestore
                            .collection('adoption_requests')
                            .document('${loggedInUser.uid}${widget.animal.id}')
                            .delete();
                      }
                    }
                    Alert(
                      context: context,
                      title: alertTitle,
                      desc: alertMessage,
                      buttons: [
                        DialogButton(
                          child: Text(
                            'Okay',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          color: Color(0xFF656176),
                        )
                      ],
                    ).show();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: screenHeight * 0.07389,
                    width: screenHeight * 0.29556,
                    decoration: BoxDecoration(
                        color: widget.animal.isAdopted
                            ? kActiveAdoptionColor
                            : kInactiveAdoptionColor,
                        borderRadius:
                            BorderRadius.circular(screenHeight * 0.01847)),
                    child: Text(
                      'Adoption',
                      style: TextStyle(
                          fontSize: screenHeight * 0.02463,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
