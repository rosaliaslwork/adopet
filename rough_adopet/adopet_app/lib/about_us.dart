import 'package:adopetapp/menu_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'team_member.dart';

class AboutUs extends StatelessWidget {
  static String id = 'about_us_page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: 50,
              left: 20,
              bottom: 30,
            ),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, MenuPage.id);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Color(0xFF656176),
                    size: 30,
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  'Our Team',
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF656176),
                  ),
                ),
              ],
            ),
          ),
          TeamMember(
            collegeName: 'Indira Gandhi Delhi\nTechnical University\nfor Women',
            firstName: 'SRISHTI',
            lastName: 'VASHISTHA',
            image: 'images/srishti-aboutus.jpg',
          ),
          SizedBox(
            height: 25.0,
          ),
          TeamMember(
            collegeName: 'Sri Ramaswamy Memorial\nUniversity, Amaravati',
            firstName: 'LAASYA',
            lastName: 'CHOUDARY',
            image: 'images/laasya-aboutus.jpeg',
          ),
          SizedBox(
            height: 25.0,
          ),
          TeamMember(
            collegeName: 'Indira Gandhi Delhi\nTechnical University\nfor Women',
            firstName: 'ROSALIA',
            lastName: 'LONGJAM',
            image: 'images/rosalia-aboutus.jpeg',
          ),
          Padding(
            padding: EdgeInsets.only(top: 30, left: 30),
            child: Text(
              'Built with',
              style: TextStyle(
                fontSize: 17,
                color: Colors.grey[700],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 30, top: 10),
                child: Image.asset(
                  'images/Google-flutter-logo.png',
                  height: 35,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 20, top: 10),
                child: Image.asset(
                  'images/transparent_adoPet_logo.png',
                  height: 50,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
