import 'package:adopetapp/about_us.dart';
import 'package:adopetapp/constants.dart';
import 'package:adopetapp/user_profile.dart';
import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'pet_catagory_card.dart';
import 'package:fluttericon/rpg_awesome_icons.dart';
import 'animal_class.dart';
import 'pet_card.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'side_bar.dart';
import 'favourites_pet_card.dart';
import 'pet_profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum WidgetMarker { dogs, cats, rabbits, birds }

class MenuPage extends StatefulWidget {
  static String id = 'menu_page';
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  final _auth = FirebaseAuth.instance;
  FirebaseUser loggedInUser;
  var scaffoldKey = GlobalKey<ScaffoldState>();

  Future<List<Animal>> search(String search) async {
    await Future.delayed(Duration(seconds: 1));
    List<Animal> matches = [];
    for (var animal in allAnimals) {
      if (animal.breed.toLowerCase().contains(search.toLowerCase())) {
        matches.add(animal);
      }
    }
    return matches;
  }

  @override
  void initState() {
    super.initState();
    getEmail();
    getCurrentUser();
  }

  void getCurrentUser() async {
    final user = await _auth.currentUser();
    try {
      if (user != null) {
        loggedInUser = user;
        print(loggedInUser.email);
      }
    } catch (e) {
      print(e);
    }
  }

  String email = '';

  Future getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      email = preferences.getString('email');
    });
    print(email);
  }

  WidgetMarker selectedWidgetMarker = WidgetMarker.dogs;

  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMarker.dogs:
        return getDogContainer();
      case WidgetMarker.cats:
        return getCatContainer();
      case WidgetMarker.rabbits:
        return getRabbitContainer();
      case WidgetMarker.birds:
        return getBirdContainer();
    }
    return getDogContainer();
  }

  Widget getDogContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: dogs,
    );
  }

  Widget getCatContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: cats,
    );
  }

  Widget getRabbitContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: rabbits,
    );
  }

  Widget getBirdContainer() {
    final deviceWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return PetCard(
      screenHeight: screenHeight,
      deviceWidth: deviceWidth,
      pets: birds,
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFe6e4ef),
      drawer: Drawer(
        child: SideBar(),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.05541,
                right: screenHeight * 0.01231,
                left: screenHeight * 0.01231,
                bottom: screenHeight * 0.01231),
            child: Container(
              color: Color(0xFFe6e4ef),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
//                      Navigator.push(context,
//                          MaterialPageRoute(builder: (context) {
//                        return SideBar();
//                      }));
                      scaffoldKey.currentState.openDrawer();
                    },
                    child: Icon(
                      FontAwesomeIcons.bars,
                      color: Color(0xFF656176),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return AboutUs();
                      }));
                    },
//                    child: CircleAvatar(
//                      radius: screenHeight * 0.025,
//                      backgroundImage: AssetImage('images/rosalia.png'),
//                    ),
                    child: Image.asset(
                      'images/adopetLogo.png',
                      height: 40,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(screenHeight * 0.03078),
                    topRight: Radius.circular(screenHeight * 0.03078)),
              ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenHeight * 0.01231,
                          vertical: screenHeight * 0.01231),
                      child: SearchBar<Animal>(
                        onSearch: search,
                        cancellationWidget: Icon(Icons.close),
                        onError: (error) {
                          return Center(
                            child: Text("Error occurred : $error"),
                          );
                        },
                        emptyWidget: Center(
                          child: Text("No search results"),
                        ),
                        onItemFound: (Animal animal, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return PetProfile(animal: animal);
                              }));
                            },
                            child: FavouritesPetCard(
                              screenHeight: screenHeight,
                              screenWidth: screenWidth,
                              name: animal.name,
                              ageUnit: animal.ageUnit,
                              ageNumber: animal.ageNumber,
                              image: animal.image,
                              gender: animal.gender,
                              breed: animal.breed,
                            ),
                          );
                        },
                        hintText: 'Search breed',
                        searchBarStyle: SearchBarStyle(
                            borderRadius: BorderRadius.all(
                                Radius.circular(screenHeight * 0.01847))),
                        placeHolder: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker = WidgetMarker.dogs;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: FontAwesomeIcons.dog,
                                    petLabel: 'Dogs',
                                    color: (selectedWidgetMarker ==
                                            WidgetMarker.dogs)
                                        ? kActiveCatagoryCardColor
                                        : kInactiveCatagoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMarker.dogs)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMarker.dogs)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker = WidgetMarker.cats;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: FontAwesomeIcons.cat,
                                    petLabel: 'Cats',
                                    color: (selectedWidgetMarker ==
                                            WidgetMarker.cats)
                                        ? kActiveCatagoryCardColor
                                        : kInactiveCatagoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMarker.cats)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMarker.cats)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker =
                                          WidgetMarker.rabbits;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: RpgAwesome.rabbit,
                                    petLabel: 'Rabbits',
                                    color: (selectedWidgetMarker ==
                                            WidgetMarker.rabbits)
                                        ? kActiveCatagoryCardColor
                                        : kInactiveCatagoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMarker.rabbits)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMarker.rabbits)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedWidgetMarker = WidgetMarker.birds;
                                    });
                                  },
                                  child: PetCategoryCard(
                                    screenHeight: screenHeight,
                                    petIcon: FontAwesomeIcons.crow,
                                    petLabel: 'Birds',
                                    color: (selectedWidgetMarker ==
                                            WidgetMarker.birds)
                                        ? kActiveCatagoryCardColor
                                        : kInactiveCatagoryCardColor,
                                    petIconColor: (selectedWidgetMarker ==
                                            WidgetMarker.birds)
                                        ? kActivePetIconColor
                                        : kInactiveIconTextColor,
                                    petTextColor: (selectedWidgetMarker ==
                                            WidgetMarker.birds)
                                        ? kActivePetTextColor
                                        : kInactiveIconTextColor,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              child: getCustomContainer(),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
