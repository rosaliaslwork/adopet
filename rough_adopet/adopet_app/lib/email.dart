import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';

sendMail(String sender, String petName, bool isWithdraw) async {
  String username = 'adopetapplication@gmail.com';
  String password = 'LaasyaRosaliaSrishti'; //passsword
  String emailContent;
  String subjectContent;
  if (isWithdraw) {
    emailContent = '$sender has withdrawn their request to adopt $petName';
    subjectContent = 'Withdrawel of adoption request from $sender';
  } else {
    emailContent = '$sender has requested to adopt $petName';
    subjectContent = 'Adoption Request from $sender';
  }

  final smtpServer = gmail(username, password);
  // Creating the Gmail server

  // Create our email message.
  final message = Message()
    ..from = Address(username)
    ..recipients.add('rosaliasteffi@gmail.com') //recipent email
    //..ccRecipients.addAll(['destCc1@example.com', 'destCc2@example.com']) //cc Recipents emails
    //..bccRecipients.add(Address('bccAddress@example.com')) //bcc Recipents emails
    ..subject = subjectContent //subject of the email
    //..text =
    //'This is the plain text.\nThis is line 2 of the text part.'
    ..html = "<h3> $emailContent </h3>\n<p></p>"; //body of the email

  try {
    final sendReport = await send(message, smtpServer);
    print(
        'Message sent: ' + sendReport.toString()); //print if the email is sent
  } on MailerException catch (e) {
    print(
        'Message not sent. \n' + e.toString()); //print if the email is not sent
    // e.toString() will show why the email is not sending
  }
}
