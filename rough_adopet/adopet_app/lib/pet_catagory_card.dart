import 'package:flutter/material.dart';

class PetCategoryCard extends StatelessWidget {
  PetCategoryCard(
      {@required this.screenHeight,
      @required this.petIcon,
      @required this.petLabel,
      @required this.color,
      @required this.petIconColor,
      @required this.petTextColor});

  final IconData petIcon;
  final String petLabel;
  final double screenHeight;
  final Color color;
  final Color petIconColor;
  final Color petTextColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenHeight * 0.09852,
      height: screenHeight * 0.09852,
      child: Card(
        elevation: screenHeight * 0.00615,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(screenHeight * 0.01847)),
        color: color,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              petIcon,
              size: screenHeight * 0.03694,
              color: petIconColor,
            ),
            SizedBox(
              height: screenHeight * 0.00615,
            ),
            Text(
              petLabel,
              style: TextStyle(
                color: petTextColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
