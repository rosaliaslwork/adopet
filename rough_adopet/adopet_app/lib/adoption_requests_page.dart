import 'package:adopetapp/menu_page.dart';
import 'package:flutter/material.dart';
import 'side_bar.dart';
import 'user_profile.dart';
import 'favourites_pet_card.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'animal_class.dart';
import 'pet_profile.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:empty_widget/empty_widget.dart';

class AdoptionRequestsPage extends StatefulWidget {
  static String id = 'adoption_requests_page';
  @override
  _AdoptionRequestsPageState createState() => _AdoptionRequestsPageState();
}

class _AdoptionRequestsPageState extends State<AdoptionRequestsPage> {
  final _firestore = Firestore.instance;
  final _auth = FirebaseAuth.instance;
  FirebaseUser loggedInUser;

  void adoptionRequestsStream() async {
    await for (var snapshot
        in _firestore.collection('adoption_requests').snapshots()) {
      for (var requests in snapshot.documents) {
        print(requests.data);
      }
    }
  }

  void getCurrentUser() async {
    final user = await _auth.currentUser();
    try {
      if (user != null) {
        loggedInUser = user;
        print(loggedInUser.email);
      }
    } catch (e) {
      print(e);
    }
  }

  String email = '';

  Future getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      email = preferences.getString('email');
    });
    print(email);
  }

  @override
  void initState() {
    super.initState();
    adoptionRequestsStream();
    getCurrentUser();
    getEmail();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xFFe6e4ef),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.05541,
                right: screenWidth * 0.02666,
                left: screenWidth * 0.02666,
                bottom: screenHeight * 0.01231),
            child: Container(
              color: Color(0xFFe6e4ef),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, MenuPage.id);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Color(0xFF656176),
                    ),
                  ),
                  Text(
                    'Adoption Requests',
                    style: TextStyle(
                      fontSize: screenHeight * 0.02832,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF656176),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return UserProfile();
                      }));
                    },
//                    child: CircleAvatar(
//                      radius: screenHeight * 0.025,
//                      backgroundImage: AssetImage('images/rosalia.png'),
//                    ),
                    child: Image.asset(
                      'images/adopetLogo.png',
                      height: 40,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(screenHeight * 0.03078),
                    topRight: Radius.circular(screenHeight * 0.03078)),
              ),
              child: StreamBuilder<QuerySnapshot>(
                stream: _firestore.collection('adoption_requests').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.blueAccent,
                      ),
                    );
                  }
                  final requests = snapshot.data.documents;
                  List<GestureDetector> adoptionRequestWidgets = [];
                  for (var request in requests) {
                    var userID =
                        loggedInUser != null ? loggedInUser.uid : email;
                    if (request.data['user-id'] == userID) {
                      Animal animal1;
                      final name = request.data['name'];
                      final breed = request.data['breed'];
                      final ageNumber = request.data['ageNumber'];
                      final ageUnit = request.data['ageUnit'];
                      final image = request.data['image'];
                      final gender = request.data['gender'];
                      for (var animal in allAnimals) {
                        if (animal.name == name) {
                          animal1 = animal;
                        }
                      }
                      final adoptionRequestWidget = GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return PetProfile(
                              animal: animal1,
                            );
                          }));
                        },
                        child: FavouritesPetCard(
                          screenHeight: screenHeight,
                          screenWidth: screenWidth,
                          name: name,
                          breed: breed,
                          ageNumber: ageNumber,
                          ageUnit: ageUnit,
                          gender: gender,
                          image: image,
                        ),
                      );
                      adoptionRequestWidgets.add(adoptionRequestWidget);
                    }
                  }
                  return adoptionRequestWidgets.length != 0
                      ? ListView(
                          children: adoptionRequestWidgets,
                        )
                      : EmptyListWidget(
                          title: 'No Requests',
                          subTitle:
                              'You haven\'t sent any adoption\nrequests yet',
                          image: 'images/loadingGhost.png',
                          titleTextStyle: Theme.of(context)
                              .typography
                              .dense
                              .headline4
                              .copyWith(color: Color(0xFF807b96)),
                          subtitleTextStyle: Theme.of(context)
                              .typography
                              .dense
                              .bodyText1
                              .copyWith(color: Color(0xFF807b96)));
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
