import 'package:adopetapp/menu_page.dart';
import 'package:flutter/material.dart';
import 'constants.dart';

class WhyAdoptPage extends StatefulWidget {
  @override
  _WhyAdoptPageState createState() => _WhyAdoptPageState();
}

class _WhyAdoptPageState extends State<WhyAdoptPage> {
  List<String> whyAdoptText = [kWhyAdoptText1, kWhyAdoptText2, kWhyAdoptText3];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Container(
                color: Color(0xFFe6e4ef),
                padding: EdgeInsets.only(top: 50.0, left: 16.0),
                alignment: Alignment.topLeft,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, MenuPage.id);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Color(0xFF656176),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.bottomCenter,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0),
                    ),
//                      color: Color(0xFFdbd5f8),
//                    color: Color(0xFFf4eeff),
                    color: Color(0xFFe6e4ef),
                  ),
                  child: Image(
                    height: 350,
//                    image: AssetImage('images/pets.png'),
                    image: AssetImage('images/whyadoptimage.png'),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10.0,
              ),
              Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.topCenter,
//                    color: Color(0xFFf4eeff),
                    child: Text(
                      'Why Adopt?',
                      style: TextStyle(
                        fontFamily: 'Pacifico-2',
                        letterSpacing: 2,
                        fontSize: 36.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF656176),
                      ),
                    ),
                  ),
                  Divider(
                    indent: 60.0,
                    endIndent: 60.0,
                    color: Color(0xFF656176),
                  )
                ],
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ListView(
                    controller: ScrollController(
                      initialScrollOffset: 40,
                    ),
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          kWhyAdoptText1,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Color(0xFF807b96),

//                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          kWhyAdoptText2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
//                              fontFamily: 'Roboto',
                            fontSize: 20.0,
                            color: Color(0xFF807b96),
                          ),
                        ),
                      ),
                      ListTile(
                          contentPadding: EdgeInsets.only(
                              right: 10, left: 16, bottom: 15.0),
                          title: Text(
                            kWhyAdoptText3,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Color(0xFF807b96),
                            ),
                          ))
                    ],
                  ),
                ),
              )
            ],
          ),
        )
      ],
    ));
  }
}
