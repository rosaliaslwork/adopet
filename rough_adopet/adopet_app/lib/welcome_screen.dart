import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'button.dart';
import 'registration_screen.dart';
import 'login_screen.dart';

class WelcomeScreen extends StatefulWidget {
  static String id = 'welcome_screen';
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SizedBox(
            height: screenHeight * 0.09852,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                FontAwesomeIcons.paw,
                color: Color(0xFF656176),
                size: screenHeight * 0.04556,
              ),
              SizedBox(
                width: screenWidth * 0.02666,
              ),
              Text(
                'adoPet',
                style: TextStyle(
                  color: Color(0xFF656176),
                  fontWeight: FontWeight.bold,
                  fontSize: screenHeight * 0.04926,
                ),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight * 0.03078,
          ),
          Center(
            child: Container(
              height: screenHeight * 0.46182,
              child: Image.asset(
                'images/welcome_image.png',
              ),
            ),
          ),
          SizedBox(
            height: screenHeight * 0.02463,
          ),
          Button(
            onPressed: () {
              Navigator.pushNamed(context, LoginScreen.id);
            },
            buttonTitle: 'Login',
            color: Color(0xFF807b96),
            textColor: Colors.white,
          ),
          Button(
            onPressed: () {
              Navigator.pushNamed(context, RegistrationScreen.id);
            },
            buttonTitle: 'Register',
            color: Color(0xFFe6e4ef),
            textColor: Color(0xFF656176),
          ),
        ],
      ),
    );
  }
}
