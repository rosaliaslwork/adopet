import 'package:flutter/material.dart';

class PetName extends StatelessWidget {
  PetName({this.screenHeight, this.petName});

  final double screenHeight;
  final String petName;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(
          left: screenHeight * 0.02463, top: screenHeight * 0.01231),
      height: screenHeight * 0.07758,
      width: double.infinity,
      child: Text(
        petName,
        style: TextStyle(
          fontFamily: 'Pacifico-2',
          letterSpacing: screenHeight * 0.00246,
          fontSize: screenHeight * 0.03694,
          fontWeight: FontWeight.bold,
          color: Color(0xFF656176),
        ),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(screenHeight * 0.03078),
          topRight: Radius.circular(screenHeight * 0.03078),
        ),
//                      color: Color(0xFFdbd5f8),
        color: Colors.white,
      ),
    );
  }
}

class PetLocation extends StatelessWidget {
  PetLocation({this.screenHeight, this.location});

  final double screenHeight;
  final String location;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.location_on,
          size: screenHeight * 0.03078,
          color: Color(0xFF656176),
        ),
        SizedBox(
          width: screenHeight * 0.00369,
        ),
        Text(
          location,
          style: TextStyle(
            fontSize: screenHeight * 0.02216,
            color: Color(0xFF807b96),
          ),
        )
      ],
    );
  }
}

class GenderCard extends StatelessWidget {
  GenderCard({this.screenHeight, this.genderIcon, this.gender});

  final double screenHeight;
  final IconData genderIcon;
  final String gender;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenHeight * 0.09852,
      height: screenHeight * 0.09852,
      child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(screenHeight * 0.01847)),
          color: Color(0xFFe6e4ef),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                genderIcon,
                size: screenHeight * 0.03694,
                color: Color(0xFF656176),
              ),
              Text(
                gender,
                style: TextStyle(
                  color: Color(0xFF807b96),
                ),
              )
            ],
          )),
    );
  }
}

class AgeWeightCard extends StatelessWidget {
  AgeWeightCard(
      {this.screenHeight, this.numberText, this.unit, this.description});

  final double screenHeight;
  final String numberText;
  final String unit;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenHeight * 0.09852,
      height: screenHeight * 0.09852,
      child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(screenHeight * 0.01847)),
          color: Color(0xFFe6e4ef),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                textBaseline: TextBaseline.alphabetic,
                crossAxisAlignment: CrossAxisAlignment.baseline,
                children: <Widget>[
                  Text(numberText,
                      style: TextStyle(
                        fontSize: screenHeight * 0.03078,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF656176),
                      )),
                  SizedBox(
                    width: screenHeight * 0.00246,
                  ),
                  Text(unit,
                      style: TextStyle(
                        fontSize: screenHeight * 0.01847,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF656176),
                      ))
                ],
              ),
              Text(
                description,
                style: TextStyle(
                  color: Color(0xFF807b96),
                ),
              )
            ],
          )),
    );
  }
}

class BreedCard extends StatelessWidget {
  BreedCard({this.screenHeight, this.breedLine1, this.breedLine2});

  final double screenHeight;
  final String breedLine1;
  final String breedLine2;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenHeight * 0.09852,
      height: screenHeight * 0.09852,
      child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(screenHeight * 0.01847)),
          color: Color(0xFFe6e4ef),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(breedLine1,
                  style: TextStyle(
                    fontSize: screenHeight * 0.01724,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF656176),
                  )),
              Text(breedLine2,
                  style: TextStyle(
                    fontSize: screenHeight * 0.01724,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF656176),
                  )),
              Text(
                'Breed',
                style: TextStyle(
                  color: Color(0xFF807b96),
                ),
              )
            ],
          )),
    );
  }
}

class ShelterOwnerInfo extends StatelessWidget {
  ShelterOwnerInfo(
      {this.screenHeight, this.imagePath, this.ownerName, this.category});

  final double screenHeight;
  final String imagePath;
  final String ownerName;
  final String category;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: screenHeight * 0.00985),
          child: CircleAvatar(
            radius: screenHeight * 0.03694,
            backgroundImage: AssetImage(imagePath),
          ),
        ),
        SizedBox(
          width: screenHeight * 0.00985,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              ownerName,
              style: TextStyle(
                color: Color(0xFF656176),
                fontSize: screenHeight * 0.02216,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              category,
              style: TextStyle(
                fontSize: screenHeight * 0.01847,
                color: Color(0xFF807b96),
              ),
            )
          ],
        )
      ],
    );
  }
}

class OwnerShelterDescription extends StatelessWidget {
  OwnerShelterDescription({this.screenHeight, this.description});

  final double screenHeight;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        controller: ScrollController(
          initialScrollOffset: screenHeight * 0.04926,
        ),
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: screenHeight * 0.00985, right: screenHeight * 0.00985),
              child: Text(
                description,
                style: TextStyle(
                  fontSize: screenHeight * 0.02032,
                  color: Color(0xFF807b96),
                ),
              )),
        ],
      ),
    );
  }
}
