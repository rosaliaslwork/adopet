import 'package:adopetapp/about_us.dart';
import 'package:adopetapp/adoption_requests_page.dart';
import 'package:adopetapp/favourites_page.dart';
import 'package:adopetapp/login_screen.dart';
import 'package:adopetapp/menu_page.dart';
import 'package:adopetapp/registration_screen.dart';
import 'package:adopetapp/rough_work.dart';
import 'package:adopetapp/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'why_adopt.dart';
import 'pet_profile.dart';
import 'package:flutter/services.dart';
import 'side_bar.dart';
import 'user_profile.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:animated_splash/animated_splash.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var email = preferences.getString('email');
  var googleUser = preferences.getString('googleUser');
  runApp(AdoPet(
    email: email,
  ));
}

class AdoPet extends StatelessWidget {
  var email;
  AdoPet({this.email});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      initialRoute: email == null ? WelcomeScreen.id : MenuPage.id,
      routes: {
        WelcomeScreen.id: (context) => WelcomeScreen(),
        RegistrationScreen.id: (context) => RegistrationScreen(),
        LoginScreen.id: (context) => LoginScreen(),
        MenuPage.id: (context) => MenuPage(),
        SideBar.id: (context) => SideBar(),
        FavouritePage.id: (context) => FavouritePage(),
        AdoptionRequestsPage.id: (context) => AdoptionRequestsPage(),
        Rough.id: (context) => Rough(),
        AboutUs.id: (context) => AboutUs(),
      },
    );
  }
}
