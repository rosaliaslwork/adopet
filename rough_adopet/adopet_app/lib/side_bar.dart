import 'package:adopetapp/about_us.dart';
import 'package:adopetapp/adoption_requests_page.dart';
import 'package:adopetapp/favourites_page.dart';
import 'package:adopetapp/menu_page.dart';
import 'user_profile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'welcome_screen.dart';
import 'why_adopt.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SideBar extends StatefulWidget {
  static String id = 'side_bar';
  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
  bool _isLoggedIn = true;
  FirebaseAuth _auth = FirebaseAuth.instance;

  _logout() {
    _googleSignIn.signOut();
    setState(() {
      _isLoggedIn = false;
    });
  }

  String email = '';

  Future getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      email = preferences.getString('email');
    });
    print(email);
  }

  @override
  void initState() {
    super.initState();
    getEmail();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color(0xFF656176),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(screenHeight * 0.01477),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return AboutUs();
                          },
                        ),
                      );
                    },
//                    child: CircleAvatar(
//                      radius: screenHeight * 0.03,
//                      backgroundImage: AssetImage('images/adopetLogo.png'),
//                    ),
                    child: Image.asset(
                      'images/sidebarAdopetLogo.png',
                      height: 45,
                    ),
                  ),
                  SizedBox(
                    width: screenHeight * 0.00985,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          email,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xFFe6e4ef),
                            fontSize: screenHeight * 0.02216,
                          ),
                        ),
                        Text(
                          'Logged in',
                          style: TextStyle(
                            fontSize: screenHeight * 0.01847,
                            color: Color(0xFFe6e4ef),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: ReusableMenuItem(
                      screenHeight: screenHeight,
                      icon: FontAwesomeIcons.home,
                      label: 'Home',
                    ),
                  ),
                  SizedBox(
                    height: screenHeight * 0.03694,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, AdoptionRequestsPage.id);
                    },
                    child: ReusableMenuItem(
                      screenHeight: screenHeight,
                      icon: FontAwesomeIcons.paw,
                      label: 'Adoption',
                    ),
                  ),
                  SizedBox(
                    height: screenHeight * 0.03694,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, FavouritePage.id);
                    },
                    child: ReusableMenuItem(
                      screenHeight: screenHeight,
                      icon: FontAwesomeIcons.solidHeart,
                      label: 'Favourites',
                    ),
                  ),
                  SizedBox(
                    height: screenHeight * 0.03694,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return WhyAdoptPage();
                      }));
                    },
                    child: ReusableMenuItem(
                      screenHeight: screenHeight,
                      icon: FontAwesomeIcons.question,
                      label: 'Why Adopt?',
                    ),
                  ),
                  SizedBox(
                    height: screenHeight * 0.03694,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, AboutUs.id);
                    },
                    child: ReusableMenuItem(
                      screenHeight: screenHeight,
                      icon: FontAwesomeIcons.infoCircle,
                      label: 'About Us',
                    ),
                  ),
                ],
              ),
              GestureDetector(
                onTap: () async {
                  Navigator.pushNamed(context, WelcomeScreen.id);
                  _logout();
                  SharedPreferences preferences =
                      await SharedPreferences.getInstance();
                  preferences.remove('email');
                  _auth.signOut();
                },
                child: ReusableMenuItem(
                  screenHeight: screenHeight,
                  icon: FontAwesomeIcons.powerOff,
                  label: 'Log Out',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ReusableMenuItem extends StatelessWidget {
  final double screenHeight;
  final IconData icon;
  final String label;

  ReusableMenuItem({this.screenHeight, this.icon, this.label});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: screenHeight * 0.00492),
      child: Row(
        textBaseline: TextBaseline.alphabetic,
        crossAxisAlignment: CrossAxisAlignment.baseline,
        children: <Widget>[
          Icon(
            icon,
            color: Color(0xFFe6e4ef),
            size: 25,
          ),
          SizedBox(
            width: screenHeight * 0.03,
          ),
          Text(
            label,
            style: TextStyle(
              color: Color(0xFFe6e4ef),
              fontWeight: FontWeight.bold,
              fontSize: screenHeight * 0.02216,
            ),
          ),
        ],
      ),
    );
  }
}
