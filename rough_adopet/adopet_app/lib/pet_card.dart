import 'package:adopetapp/pet_profile.dart';
import 'package:flutter/material.dart';
import 'animal_class.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PetCard extends StatelessWidget {
  PetCard({this.screenHeight, this.deviceWidth, this.pets});

  final double screenHeight;
  final double deviceWidth;
  final List pets;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          itemCount: pets.length,
          itemBuilder: (context, index) {
            final animal = pets[index];
            return GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return PetProfile(animal: animal);
                }));
              },
              child: Padding(
                padding: EdgeInsets.only(
                  bottom: screenHeight * 0.01231,
                  right: deviceWidth * 0.05333,
                  left: deviceWidth * 0.05333,
                ),
                child: Stack(
                  alignment: Alignment.centerLeft,
                  children: <Widget>[
                    Material(
                      borderRadius:
                          BorderRadius.circular(screenHeight * 0.02463),
                      elevation: screenHeight * 0.00492,
                      color: Colors.grey[100],
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: screenHeight * 0.02463,
                          horizontal: deviceWidth * 0.05333,
                        ),
                        child: Row(
                          children: <Widget>[
                            SizedBox(
                              width: deviceWidth * 0.37333,
                            ),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Text(
                                        animal.name,
                                        style: TextStyle(
                                          fontFamily: 'Pacifico-2',
                                          letterSpacing: screenHeight * 0.00246,
                                          fontSize: screenHeight * 0.02586,
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFF656176),
                                        ),
                                      ),
                                      Icon(
                                        (animal.gender == 'Female')
                                            ? FontAwesomeIcons.venus
                                            : FontAwesomeIcons.mars,
                                        color: Color(0xFF656176),
                                      )
                                    ],
                                  ),
                                  Text(
                                    animal.breed,
                                    style: TextStyle(
                                      color: Color(0xFF807b96),
                                    ),
                                  ),
                                  Text(
                                    '${animal.ageNumber} ${animal.ageUnit}',
                                    style: TextStyle(
                                      color: Color(0xFF807b96),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  screenHeight * 0.02463)),
                          height: screenHeight * 0.23399,
                          width: deviceWidth * 0.4,
                        ),
                        ClipRRect(
                          borderRadius:
                              BorderRadius.circular(screenHeight * 0.02463),
                          child: Image(
                            image: AssetImage(animal.image),
                            height: screenHeight * 0.20935,
//                            fit: BoxFit.fitHeight,
                            fit: BoxFit.cover,
                            width: deviceWidth * 0.4,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }
}
